// ==UserScript==
// @name         Goodwood Twitch drop bot
// @namespace    http://tampermonkey.net/
// @version      2.16
// @description  Monitors a Twitch page for streams with drops enabled and watches them.
//               Opens first stream found and monitors the drop status,
//               returns if drops tag no longer present.
//
//               Changelog:
//               2.16    Bugfix: Forgot to reset timers when resetting watching state, would immediately blacklist next stream after resuming due to timer elapsing
//               2.15    Bugfix: Twitch changed live indicator property
//               2.14    Bugfix: Search page monitoring would not properly reload the page due to logic error, and stream tabs would not be closed after finishing stream due to another logic error
//               2.13    Bugfix: Removing from other list when adding to a block list was broken. Internal fix for default value appearing in some cases.
//               2.12    Bugfix: Reading of watching state implicitly set the watching local store entry, which would trip up isWatching. Rewrote logic in getSetting.
//               2.11    Bugfix: Field setting during GUI load was wrong and would break functionality, removing from opposite list introduced in 2.08 was broken as well, fixed that too
//               2.10    Drops enabled tag is not refreshed on stream page, changed logic to run in monitor drops page
//               2.09    Bugfix: loopPause value was in ms, erroneously multiplied delay by 1000
//               2.08    Adding current stream to list with GUI option will also remove from the other list (if found) now
//               2.07    Bugfix: Set refresh timer wrong for some functions
//               2.06    Bugfix: Fetching block lists from storage when opening GUI. Opening GUI would show stale data if values were edited in different tab.
//               2.05    Added GUI option for black / white listing of current stream
//               2.04    Sorted Black and White lists for faster lookup, using binary search and insert
//               2.03    Migrated config to GM_config library to support black and white list editing
//               2.02    Bugfix: Search page would not reload if blacklisted stream is only stream available
//               2.01    Bugfix: monitorDropsPage breaks if set up with delay since it listens for mutation
//               2.00    Added black- and white list feature. Streams would be started with drops tag and quickly turn it off, probably to gain viewers. Stream page does not update the tag without reloading the page once so if not on whitelist, we refresh the page after some time.
//               1.05    Fix: Force reload of search page after returning to it to refresh items before opening links again. Some times the stream went away but search page was not refreshed and had stale data.
//               1.04    Fix: Menu would be built before config values were updated resulting in menu text displaying with default values
//               1.03    Added menu option to reset watching state, and option to set initial wait for page to finish loading
//               1.02    Fix: Search page reload was broken, see https://stackoverflow.com/a/10840058
//               1.01    Added current value to settings dialogues
//               1.00    Initial "release"
// @author       shajnf
// @updateURL    https://gitlab.com/shajnf/goodwood/-/raw/main/goodwood.twitch.drop.bot.user.js
// @downloadURL  https://gitlab.com/shajnf/goodwood/-/raw/main/goodwood.twitch.drop.bot.user.js
// @match        https://*.twitch.tv/*
// @icon         https://www.google.com/s2/favicons?domain=twitch.tv
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_info
// @grant        GM_registerMenuCommand
// @run-at       document-idle
// ==/UserScript==

var _debug=true

// Regex for url with drops enabled tag, e.g. search page
var _matchSearchUrl = /https:\/\/.*\.twitch\.tv\/.*[\?&]tl=c2542d6d-cd10-4532-919b-3d19f30a768b/
//_matchSearchUrl = /https:\/\/.*\.twitch\.tv\/.*[\?&]tl=c2542d6d-cd10-4532-919b-3d19f30a768c/
var _matchFirstStreamClassCount = 'count(//div[@data-target="directory-first-item"])'
var _matchFirstStreamHref = '//div[@data-target="directory-first-item"]//a[@data-a-target="preview-card-image-link"]/@href'
var _matchOtherStreamsHref = '//div[@data-target="" and starts-with(@style,"order: ")]//a[@data-a-target="preview-card-image-link"]/@href'
var _matchStreamByLink = 'count(//div[starts-with(@style,"order: ")]//a[@data-a-target="preview-card-image-link" and @href="/' //+ nameofstream + '"])'
var _matchStreamDropsEnabledTagCount = 'count(//div[@class="channel-info-content"]//a[@href="/directory/all/tags/c2542d6d-cd10-4532-919b-3d19f30a768b"])'
var _matchStreamLiveTag = 'count(//div[@class="channel-info-content"]//a[@status="live"])'

var _prefix = "goodwood_"

// Config settings
var _gmFields = {
    'filter':{
        'listMode': {
            'section'  : [
                'Filter settings',
            ],
            'label'    : 'Filter streams',
            'title'    : 'Goodwood can ignore streams when monitoring for active streams. Blacklist will watch any streams not in blacklist, whitelist will only watch streams in whitelist. Grace period will monitor unknown streams and if they remove drops tag before a grace period expires add them to the blacklist.',
            'labelPos' : 'left',
            'type'     : 'radio',
            'options'  : [
                'Off',
                'Blacklist',
                'Grace period',
                'Whitelist',
            ],
            'default'  : 'Off',
        },
        'tagPresentGracePeriod': {
            'label'    : 'Grace period (minutes)',
            'title'    : 'If a stream not in the white list removes drops enabled tag before the grace period runs out, it is added to the black list.',
            'labelPos' : 'left',
            'type'     : 'int',
            'min'      : 1,
            'default'  : 3,
        },
        'whitelistDummy': {
            'label'    : 'White list',
            'title'    : 'Streams white listed for watching. Format is a list of the stream names as in the stream URL, with the twitch.tv/ part removed.',
            'labelPos' : 'left',
            'type'     : 'textarea',
            'save'     : false,
            'default'  : 'This default should never show!',
        },
        'whitelist': {
            'type'     : 'hidden',
            'default'  : JSON.stringify([],null,2),
        },
        'blacklistDummy': {
            'label'    : 'Black list',
            'title'    : 'Streams black listed for watching. Format is a list of the stream names as in the stream URL, with the twitch.tv/ part removed.',
            'labelPos' : 'left',
            'type'     : 'textarea',
            'save'     : false,
            'default'  : 'This default should never show!',
        },
        'blacklist': {
            'type'     : 'hidden',
            'default'  : JSON.stringify([],null,2),
        },
        'dummyDummy': {
            'section'  : [
                'List format example',
            ],
            'label'    : 'Format you lists like this:',
            'title'    : 'Example of how a valid list is formatted',
            'labelPos' : 'left',
            'type'     : 'textarea',
            'save'     : false,
            'default'  : JSON.stringify([
                'valid_stream',
                'anotherStream',
                '/invalid_stream',
            ],null,2),
        },
    },
    'refresh': {
        'searchRefresh': {
            'section'  : [
                'Reload and other timers',
            ],
            'label'    : 'Refresh search page (seconds)',
            'title'    : 'How often to reload the search page while monitoring for streams to watch, or while stream being watched is in grace period',
            'labelPos' : 'left',
            'type'     : 'int',
            'min'      : 10,
            'default'  : 60,
        },
        'dropRefresh': {
            'label'    : 'Refresh drops page (seconds)',
            'title'    : 'How often to reload the drops page while monitoring for drops to claim',
            'labelPos' : 'left',
            'type'     : 'int',
            'min'      : 10,
            'default'  : 60,
        },
        'initialPageLoadWait': {
            'label'    : 'Initial page load wait (seconds)',
            'title'    : 'How long to wait for the page to load before looking for streams / drops / etc. Twitch page can be slow to load and if Goodwood runs too quickly there will be no streams yet.',
            'labelPos' : 'left',
            'type'     : 'float',
            'min'      : 0.1,
            'default'  : 10,
        },
        'loopPause': {
            'label'    : 'Internal wait (ms)',
            'title'    : 'How often to loop internally while waiting for stuff to happen',
            'labelPos' : 'left',
            'type'     : 'int',
            'min'      : 10,
            'default'  : 1000,
        },
    },
}
var _gmId = {
    'filter' : 'goodwood_list',
    'refresh': 'goodwood_timers',
}
var conf = {}
conf.list = new GM_configStruct({
    'id'    : _gmId.filter,
    'fields': _gmFields.filter,
})
conf.timers = new GM_configStruct({
    'id'    : _gmId.refresh,
    'fields': _gmFields.refresh,
})

var _gmEvents = {
    'filter': {
        'init': function(){
            let _gm = conf.list
            _gm.set('whitelistDummy', _gm.get('whitelist'))
            _gm.set('blacklistDummy', _gm.get('blacklist'))
        },
        'open': function(doc){
            let _gm = conf.list
            // Use a listener to update the hidden field when the dummy field passes validation
            _gm.fields['whitelistDummy'].node.addEventListener('change', function(){
                listTextFieldChangeListener(_gm,'whitelistDummy')
            }, false)
            _gm.fields['blacklistDummy'].node.addEventListener('change', function(){
                listTextFieldChangeListener(_gm, 'blacklistDummy')
            }, false)
            // Set the values of the dummy fields to the saved value:
            if(_debug){console.log("[ Goodwood ] GM_config open: Setting list field text contents...")}
            _gm.fields['whitelistDummy'].node.textContent = _gm.get('whitelist')
            if(_debug){console.log("[ Goodwood ] GM_config save: whitelistDummy set to \n"+_gm.get('whitelist'))}
            _gm.fields['blacklistDummy'].node.textContent = _gm.get('blacklist')
            if(_debug){console.log("[ Goodwood ] GM_config save: blacklistDummy set to \n"+_gm.get('blacklist'))}
        },
        'save': function(values) {
            if(_debug){console.log("[ Goodwood ] GM_config save: checking fields...")}
            let _gm = conf.list
            let storeId
            let list
            let valid
            for (let id in values) {
                switch (id) {
                    case 'whitelistDummy':
                    case 'blacklistDummy':
                        if(_debug){console.log("[ Goodwood ] GM_config save: Field: "+id)}
                        // remove Dummy from id to get hidden field to store to:
                        storeId = id.substring(0,id.length-5)
                        // Fetch unsaved value in text field:
                        list = _gm.get(id, true)
                        if(_debug){console.log("[ Goodwood ] GM_config save: Value: "+list)}
                        // check if value is a valid array:
                        valid = listTextFieldValidator(list)
                        if (valid) {
                            // Sort and save value in correct field:
                            list = JSON.parse(list)
                            list.sort()
                            let str = JSON.stringify(list, null, 2)
                            if(_debug){console.log("[ Goodwood ] GM_config save: "+id+" field set to "+str)}
                            _gm.set(storeId, str)
                            if (_gm.fields[id].node !== null){
                                _gm.fields[id].node.textContent = str
                            }
                        } else {
                            if(_debug){console.log("[ Goodwood ] GM_config save: "+id+" field failed validation, was "+list)}
                            alert(storeId+" field invalid, value was not saved!")
                        }
                        break
                }
            }
            // Write changes to storage as well
            if(_debug){console.log("[ Goodwood ] GM_config save: Writing changes to storage")}
            _gm.write()
        },
    },
    'refresh': {
    },
}

function listTextFieldChangeListener(gmHandle,fieldName){
    if(_debug){console.log("[ Goodwood ] GM_config edit: "+fieldName+" changed, checking...")}
    // get the current value of the visible dummy field
    let list = gmHandle.get(fieldName, true)
    // is valid json? (sort on save)
    let valid = listTextFieldValidator(list)

    // Update visuals
    if (!valid){
        if(_debug){console.log("[ Goodwood ] GM_config edit: "+fieldName+" field is invalid")}
    } else {
        if(_debug){console.log("[ Goodwood ] GM_config edit: "+fieldName+" field is valid")}
    }
}

function listTextFieldValidator(text){
    let valid = true
    try {
        text = JSON.parse(text)
    } catch (e) {
        if (e instanceof SyntaxError){
            if(_debug){console.log("[ Goodwood ] GM_config validation: Field was not valid JSON")}
            // invalid JSON
            valid = false
        }
    }
    if (! Array.isArray(text)){valid = false}
    return valid
}

function addStreamToList(_list, _stream){
    let _otherList = (_list=='blacklist'? 'whitelist' : 'blacklist')
    state.fetchBlocklists()
    state.appendSetting(_list, _stream)
    remove(state[_otherList], _stream)
    if(_debug){console.log("[ Goodwood ] addStreamToList: Removed "+_stream+" from other array: "+state[_otherList])}
    state.setBlocklist(_otherList, state[_otherList])
}

var state = {
    blacklist:              [], //      List of streams not to watch
    whitelist:              [], //      List of streams approved for watching
    watching:               false, //   when in list mode and evaluating whether to put on list or not: channel name, else true

    // Gets setting, checking if it is in localstorage
    getSetting: function (_key){
        let __key = _prefix+_key
        let __isArray = Array.isArray(this[_key])
        switch (_key) {
            case 'blacklist':
            case 'whitelist':
                this.fetchBlocklists()
                return this[_key]
                break
            case 'watching':
                if (!window.localStorage.hasOwnProperty(__key)){
                    return null
                }
                break
            default:
                break
        }
        if (!window.localStorage.hasOwnProperty(__key)){
            if (__isArray){
                window.localStorage.setItem(__key, JSON.stringify(this[_key]))
            } else {
                window.localStorage.setItem(__key, this[_key])
            }
        } else {
            if (__isArray){
                this[_key] = JSON.parse(window.localStorage.getItem(__key))
            } else {
                this[_key] = window.localStorage.getItem(__key)
            }
        }
        return this[_key]
    },
    // helper to set config values
    setSetting: function(_key, _val){
        if (_key == 'blacklist' || _key == 'whitelist'){
            this.setBlocklist(_key, _val)
        } else {
            let __key = _prefix+_key
            this[_key] = _val
            if (Array.isArray(_val)){
                _val = JSON.stringify(_val)
            }
            window.localStorage.setItem(__key, _val)
        }
    },

    removeSetting: function(_key){
        let __key = _prefix+_key
        window.localStorage.removeItem(__key)
    },

    appendSetting: function(_key, _val){
        let __key = _prefix+_key
        switch (_key) {
            case 'whitelist':
            case 'blacklist':
                this.fetchBlocklists()
                insert(this[_key], _val)
                this.setBlocklist(_key, JSON.stringify(this[_key], null, 2))
                break
            default:
                if(_debug){console.log("[ Goodwood ] Appending to array")}
                if(_debug){console.log("[ Goodwood ] Before: "+JSON.stringify(this[_key]))}
                if(_debug){console.log("[ Goodwood ] Appending: "+_val)}
                this[_key] = [].concat(this[_key], _val)
                if(_debug){console.log("[ Goodwood ] After: "+JSON.stringify(this[_key]))}
                window.localStorage.setItem(__key, JSON.stringify(this[_key]))
                break
        }
    },

    isWatching: function(){
        return window.localStorage.hasOwnProperty(_prefix+"watching")
    },

    // Refresh blocklists with values from GM_config
    fetchBlocklists: function(){
        this.whitelist = JSON.parse(conf.list.get('whitelist'))
        this.blacklist = JSON.parse(conf.list.get('blacklist'))
    },
    setBlocklist: function(_key, _val){
        switch (_key){
            case 'blacklist':
            case 'whitelist':
                if (Array.isArray(_val)){
                    _val = JSON.stringify(_val, null, 2)
                }
                conf.list.set(_key, _val)
                conf.list.set(_key+"Dummy",_val)
                conf.list.save()
                break
            default:
                break
        }
    },

    // Is stream allowed?
    // Returns false if in blacklist or if in whitelist only mode and not in list,
    // else returns 'white' or 'grey' depending on whether stream is in whitelist or not
    // Does not refresh blocklist values from localstorage for performance reasons, call fetchBlocklists before this function
    listsAllow: function(_streamUrl){
        let _streamName = _streamUrl.substring(1)
        if (contains(this.blacklist,_streamName)) {return false}

        if (contains(this.whitelist,_streamName)) {return 'white'}
        else if (conf.list.get('listMode') == "Whitelist") {return false} //not in whitelist, but in whitelist mode
        else return 'grey'
    },

    // stolen from https://github.com/kiranmurmuone/auto-close-twitch-ads
    scriptName: function (_key) {
        if (typeof GM_info !== "undefined") {
            if (_key) {
                return GM_info.script.name.replace(new RegExp(" ", "g"), "-");
            } else {
                return GM_info.script.name;
            }
        }
        return "undefined";
    },
    timers: {
        watchStream: null,
        gracePeriod: null,
    },
}

// Close tab if stream watching flag is removed or stream has otherwise ended
function watchStream(){
    if(_debug){console.log("[ Goodwood ] watchStream: Setting interval to check for stream end.")}
    state.timers.watchStream = setInterval(watchStreamEnd,conf.timers.get('loopPause'))
}
function watchStreamEnd(){
    let _streamLive = Boolean(document.evaluate(_matchStreamLiveTag, document, null, XPathResult.NUMBER_TYPE, null).numberValue)

    if (!_streamLive){
        if(_debug){console.log("[ Goodwood ] This stream is no longer live, stopping watching it")}
        state.removeSetting('watching')
    }

    if (!state.isWatching()){
        if(_debug){console.log("[ Goodwood ] No longer watching this stream, closing tab")}
        clearInterval(state.timers.watchStream)
        window.close()
    }
}

// Monitors search page for current stream, if
function monitorStream() {
    let _watching = state.getSetting("watching")
    if(_debug){console.log("[ Goodwood ] Watching flag: "+_watching)}
    let _name = (_watching[0]=="?") ? _watching.substring(1) : _watching

    // Search for stream we are currently watching among the streams listed as drops enabled
    let _dropsEnabled = Boolean(document.evaluate(_matchStreamByLink + _name + '"])', document, null, XPathResult.NUMBER_TYPE, null).numberValue)
    if(_debug){console.log("[ Goodwood ] Found current stream ("+_name+") listed among drops enabled streams: "+_dropsEnabled)}

    if ( _dropsEnabled ){
        if (_watching[0]=='?') {
            if(_debug){console.log("[ Goodwood ] Evaluating unknown stream, grace period "+conf.list.get('tagPresentGracePeriod')+"min, first seen "+state.timers.gracePeriod)}
            if (state.timers.gracePeriod == null){
                state.timers.gracePeriod = new Date(Date.now() + conf.list.get('tagPresentGracePeriod')*60000)
            } else if (state.timers.gracePeriod <= Date.now()){
                // Grace period ran out and stream still has drops, whitelisting?
                if(_debug){console.log("[ Goodwood ] Stream grace period elapsed and drops are still enabled.")}
                state.setSetting('watching', _name)
                state.timers.gracePeriod = null
            }
        } else {
            if(_debug){console.log("[ Goodwood ] Checking back on stream, drops still present")}
        }
    } else {
        if(_debug){console.log("[ Goodwood ] Stream no longer has drops or went offline")}
        // Drops tag dissapeared while in checking mode, blacklist stream
        if (_watching[0]=="?"){
            if(_debug){console.log("[ Goodwood ] Stream "+_name+" seemed to have drops tag for a very short time, adding to blacklist")}
            state.appendSetting("blacklist", _name)
        }
        state.removeSetting("watching")
    }
    setTimeout(window.location.reload.bind(window.location), conf.timers.get('searchRefresh')*1000);
}

// Watches page for available streams, opens any found in new tab
// Also opens tab with inventory to claim drops
// If already watching a stream, does not refresh and open new tabs
// but monitors page to check if stream disappears from search results
function monitorSearchPage() {
    let streamUrl

    if(_debug){console.log("[ Goodwood ] Already watching: "+state.isWatching())}
    // If not already watching stream
    if (!state.isWatching()){
        // Checking if any streams found
        let _streamsAvailable = Boolean(document.evaluate(_matchFirstStreamClassCount, document, null, XPathResult.NUMBER_TYPE, null).numberValue)
        if(_debug){console.log("[ Goodwood ] Streams available: "+_streamsAvailable)}

        if (_streamsAvailable){
            if(_debug){console.log("[ Goodwood ] Streams with drops found, fetching first one")}
            let _foundStream = true

            // Fetch first stream url
            streamUrl = document.evaluate(_matchFirstStreamHref, document, null, XPathResult.STRING_TYPE, null).stringValue

            // Check against lists?
            if (conf.list.get('listMode') != "Off"){
                if(_debug){console.log("[ Goodwood ] List mode "+conf.list.get('listMode')+", loading lists")}
                state.fetchBlocklists()
                _foundStream = state.listsAllow(streamUrl)
                // Try other streams if first one is not allowed
                if (!_foundStream){
                    let _res=document.evaluate(_matchOtherStreamsHref, document, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null)

                    let _node
                    // iterate over streams in list until an allowed one is found
                    while ( _node = _res.iterateNext()){
                        streamUrl = _node.value
                        _foundStream = state.listsAllow(streamUrl)
                        if (_foundStream){
                            break
                        }
                    }
                }
            }

            // Have we found a stream? Start watching it
            if (_foundStream){
                if(_debug){console.log("[ Goodwood ] Stream link: "+streamUrl+" Watching it in "+_foundStream+" mode")}

                // Set watching stream status
                if (conf.list.get('listMode') == "Grace period" && _foundStream == 'grey'){
                    if(_debug){console.log("[ Goodwood ] Setting watching status to ?"+streamUrl.substring(1))}
                    state.setSetting("watching", "?".concat(streamUrl.substring(1)))
                } else {
                    state.setSetting("watching",streamUrl.substring(1))
                }
                if(_debug){console.log("[ Goodwood ] Set stream watching flag, opening tabs")}

                // open tabs
                window.open("https://www.twitch.tv/drops/inventory")
                window.open(streamUrl)

                //DEBUG TODO remove
                //setTimeout(state.removeSetting("watching"), 5000)
                if(_debug){console.log("[ Goodwood ] Reloading function in "+conf.timers.get('loopPause')+"ms")}
                setTimeout(monitorStream, conf.timers.get('loopPause'))
            } else {
                if(_debug){console.log("[ Goodwood ] No applicable streams found, reloading in "+conf.timers.get('searchRefresh')+"s")}
                setTimeout(window.location.reload.bind(window.location), conf.timers.get('searchRefresh')*1000);
            }
        } else {
            // No streams abailable or none fitting current list mode...
            if(_debug){console.log("[ Goodwood ] No applicable streams found, reloading in "+conf.timers.get('searchRefresh')+"s")}
            setTimeout(window.location.reload.bind(window.location), conf.timers.get('searchRefresh')*1000);
        }
    } else { // Watching stream, wait for stream to end
        monitorStream()
    }
}

// Claims drops in the inventory page if they appear
// Avoids unnecessary page reloads while not watching streams
// Closes tab after final check when no longer watching stream
//
// Stolen and adapted from bAdRocK https://greasyfork.org/nb/scripts/420346-auto-claim-twitch-drop
function monitorDropsPage() {
    const claimButton = '[data-test-selector="DropsCampaignInProgressRewardPresentation-claim-button"]';
    var onMutate = function(mutationsList) {
        mutationsList.forEach(mutation => {
            if(document.querySelector(claimButton)) document.querySelector(claimButton).click();
        })
    }
    var observer = new MutationObserver(onMutate);
    observer.observe(document.body, {childList: true, subtree: true});

    setTimeout(function() {
        if (state.isWatching()){
            window.location.reload();
        } else {
            window.close()
        }
    }, conf.timers.get('dropRefresh')*1000);
}

// Set config values if not set already
function initConfig(){
    //    if(_debug){console.log("[ Goodwood ] initConfig: whitelist is: "+conf.list.get('whitelist'))}
    conf.list.init({
        'id'     : _gmId.filter,
        'events' : _gmEvents.filter,
    })
    //    if(_debug){console.log("[ Goodwood ] GM_config list built: whitelist is: "+conf.list.get('whitelist'))}
    conf.timers.init({
        'id'     : _gmId.refresh,
        'events' : _gmEvents.refresh,
    })

    state.fetchBlocklists()
}

// Config menu settings
function registerMenuCommands(){
    if (typeof GM_registerMenuCommand !== "undefined") {
        GM_registerMenuCommand(state.scriptName() + ": Blacklist current stream", function(){addStreamToList('blacklist',window.location.pathname.substring(1))},'b');
        GM_registerMenuCommand(state.scriptName() + ": Whitelist current stream", function(){addStreamToList('whitelist',window.location.pathname.substring(1))},'w');
        GM_registerMenuCommand(state.scriptName() + ": Edit filter settings", function(){conf.list.open()},'f');
        GM_registerMenuCommand(state.scriptName() + ": Edit timer settings", function(){conf.timers.open()},'t');
        GM_registerMenuCommand(state.scriptName() + ": Reset watching status", function(){
            state.removeSetting("watching")
            state.timers.gracePeriod = null
        },'r');
    }
}


// Helper functions for sorted arrays of strings:

function contains(_arr, _str){
    return -1 < indexOf(_arr, _str)
}
// Binary search for element
function indexOf(_arr, _str){
    let start = 0
    let end = _arr.length -1

    while (start <= end){
        let middle = Math.floor((start+end)/2)
        let _cmp = String(_arr[middle]).localeCompare(_str)
        // _arr[middle] == _str
        if (_cmp == 0){
            return middle
            // _arr[middle] < _str
        } else if (_cmp < 0) {
            start = middle + 1
            // _arr[middle] > _str
        } else {
            end = middle - 1
        }
    }
    return -1
}
// Binary insert into array:
// (no duplicates)
function insert(_arr, _str){
    if (_str.length = 0){
        throw new SyntaxError('Empty string cannot be inserted')
    }
    if (!Array.isArray(_arr)){
        if (_arr.length = 0) {
            throw new SyntaxError('Array cannot be an empty string')
        }
        _arr = [].concat(_arr)
    }
    let start = 0
    let end = _arr.length - 1

    while (end - start > -1){
        let middle = Math.floor((start+end)/2)
        let _cmp = String(_arr[middle]).localeCompare(_str)

        // _arr[middle] == _str
        if (_cmp == 0) {
            if(_debug){console.log("[ Goodwood ] Array binary insert: Array contained element "+_str+" already")}
            return
            // _arr[middle] < _str
        } else if (_cmp < 0) {
            start = middle + 1
            // _arr[middle] > _str
        } else {
            end = middle - 1
        }
    }
    _arr.splice(start, 0, _str)
}

function remove(_arr, _str){
    if(_debug){console.log("[ Goodwood ] Array binary remove: Array is "+_arr)}
    if(_debug){console.log("[ Goodwood ] Array binary remove: String to remove is "+_str)}
    let index = indexOf(_arr,_str)
    if (index < 0){
        if(_debug){console.log("[ Goodwood ] Array binary remove: Array did not contain string")}
        return _arr
    } else {
        _arr.splice(index,1)
        return _arr
    }
}


// MAIN
(function() {
    'use strict';
    initConfig()
    registerMenuCommands()

    let watching = state.isWatching()
    if (watching){
        watching = state.getSetting('watching')
    }
    let currentUrl = window.location
    let streamName = window.location.pathname.substring(1)
    if(_debug){console.log("[ Goodwood ] Current URL: "+currentUrl+", stream name "+streamName+" (flag: "+watching+")")}

    if ( _matchSearchUrl.test(currentUrl) ) {
        if(_debug){console.log("[ Goodwood ] Search page pattern matched, calling monitorSearchPage()")}
        setTimeout(monitorSearchPage, conf.timers.get('initialPageLoadWait')*1000)
    } else if ( currentUrl == 'https://www.twitch.tv/drops/inventory' ) {
        if(_debug){console.log("[ Goodwood ] Inventory page open, calling monitorDropsPage()")}
        monitorDropsPage()
    } else if (streamName == watching || "?".concat(streamName) == watching) {
        if(_debug){console.log("[ Goodwood ] Not search page or inventory, but stream watching flag set to this stream ("+streamName+"). Calling watchStream().")}
        setTimeout(watchStream, conf.timers.get('initialPageLoadWait')*1000)
    }
})();


// Following shamelessly stolen from dinglemyberry#6969 https://greasyfork.org/en/scripts/400540-valorantdropfarmer/ Tricking Twitch to think that the stream is never tabbed off
// Try to trick the site into thinking it's never hidden
Object.defineProperty(document, 'hidden', {value: false, writable: false});
Object.defineProperty(document, 'visibilityState', {value: 'visible', writable: false});
Object.defineProperty(document, 'webkitVisibilityState', {value: 'visible', writable: false});
document.dispatchEvent(new Event('visibilitychange'));
document.hasFocus = function () { return true; };

// visibilitychange events are captured and stopped
document.addEventListener('visibilitychange', function(e) {
    e.stopImmediatePropagation();
}, true, true);

// Set player quality to lowest
window.localStorage.setItem('s-qs-ts', Math.floor(Date.now()));
window.localStorage.setItem('video-quality', '{"default":"160p30"}');
window.localStorage.setItem('video-muted', '{"default":true}');





